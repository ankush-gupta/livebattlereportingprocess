package com.qurekapro.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

import com.qurekapro.service.ReportingService;

@SpringBootApplication
@ComponentScan("com.qurekapro")
@EnableAsync
public class LivebattleProcessReporting implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(LivebattleProcessReporting.class, args);
	}

//	@Autowired
//	PropertiesConfig propConfig;

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@Autowired
	ReportingService service;

	@Override
	public void run(String... args) throws Exception {
		service.generateHourlyReport();
	}

}
