package com.qurekapro.dao;

import java.util.List;
import java.util.Map;

import com.qurekapro.dto.LivebattleAmountWonPecentDto;
import com.qurekapro.dto.LivebattleUserNetwinsDto;
import com.qurekapro.dto.UserCommonPoolDto;
import com.qurekapro.dto.WinnersDto;

public interface LivebattleDao {

	public List<LivebattleUserNetwinsDto> getLivebattleUserNetwinsData(String added_date);

	public List<LivebattleAmountWonPecentDto> getLivebattleUserNetMarginAndWinPerData(String added_date);

	public List<UserCommonPoolDto> getLivebattleUserMatchMakingSessionData(String added_date);

	public List<WinnersDto> getLivebattleWinnersCheckReportData(String added_date);

	public List<Map<String, Object>> getNetWinsLimitGameAndFormatWise();
}
