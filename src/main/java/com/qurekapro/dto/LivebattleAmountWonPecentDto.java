package com.qurekapro.dto;

public class LivebattleAmountWonPecentDto {
	
	private long uid;
	private int wins;
	private int loses;
	private int net_wins;
	private long tot_enty_fee;
	private long tot_won_amount;
	public long getUid() {
		return uid;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public int getWins() {
		return wins;
	}
	public void setWins(int wins) {
		this.wins = wins;
	}
	public int getLoses() {
		return loses;
	}
	public void setLoses(int loses) {
		this.loses = loses;
	}
	public int getNet_wins() {
		return net_wins;
	}
	public void setNet_wins(int net_wins) {
		this.net_wins = net_wins;
	}
	public long getTot_enty_fee() {
		return tot_enty_fee;
	}
	public void setTot_enty_fee(long tot_enty_fee) {
		this.tot_enty_fee = tot_enty_fee;
	}
	public long getTot_won_amount() {
		return tot_won_amount;
	}
	public void setTot_won_amount(long tot_won_amount) {
		this.tot_won_amount = tot_won_amount;
	}
}
