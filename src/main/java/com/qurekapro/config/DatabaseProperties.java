package com.qurekapro.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DatabaseProperties {
	@Value("${reporting.con.url}")
	private String reporting_mongo_db_url;

	@Value("${app.datasource.driver}")
	private String sql_driver;

	@Value("${primary.datasource.url}")
	private String primary_sql_db_url;

	@Value("${primary.datasource.username}")
	private String primary_sql_db_userName;

	@Value("${primary.datasource.password}")
	private String primary_sql_db_password;

	public String getReporting_mongo_db_url() {
		return reporting_mongo_db_url;
	}

	public void setReporting_mongo_db_url(String reporting_mongo_db_url) {
		this.reporting_mongo_db_url = reporting_mongo_db_url;
	}

	public String getSql_driver() {
		return sql_driver;
	}

	public void setSql_driver(String sql_driver) {
		this.sql_driver = sql_driver;
	}

	public String getPrimary_sql_db_url() {
		return primary_sql_db_url;
	}

	public void setPrimary_sql_db_url(String primary_sql_db_url) {
		this.primary_sql_db_url = primary_sql_db_url;
	}

	public String getPrimary_sql_db_userName() {
		return primary_sql_db_userName;
	}

	public void setPrimary_sql_db_userName(String primary_sql_db_userName) {
		this.primary_sql_db_userName = primary_sql_db_userName;
	}

	public String getPrimary_sql_db_password() {
		return primary_sql_db_password;
	}

	public void setPrimary_sql_db_password(String primary_sql_db_password) {
		this.primary_sql_db_password = primary_sql_db_password;
	}
}
