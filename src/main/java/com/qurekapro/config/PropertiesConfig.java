package com.qurekapro.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesConfig {

	@Value("${live.battle.report.export.url}")
	private String reportExportUrl;
	
	
	
	@Value("${user.net.wins.check.header}")
	private String userNetWinsCheckHeader;
	
	@Value("${user.net.wins.check.extension}")
	private String userNetWinsCheckExtension;
	
	
	
	@Value("${user.net.margin.win.per.check.header}")
	private String userNetMarginWinPerCheckHeader;
	
	@Value("${user.net.margin.win.per.check.extension}")
	private String userNetMarginWinPerCheckExtension;
	
	
	
	@Value("${matchmaking.session.check.header}")
	private String matchmakingSessionCheckHeader;
	
	@Value("${matchmaking.session.check.extension}")
	private String matchmakingSessionCheckExtension;
	
	
	
	@Value("${winners.collection.data.header}")
	private String winnerCollectionDataHeader;
	
	@Value("${winners.collection.data.extension}")
	private String winnerCollectionDataExtension;
	
	

	public String getReportExportUrl() {
		return reportExportUrl;
	}

	public void setReportExportUrl(String reportExportUrl) {
		this.reportExportUrl = reportExportUrl;
	}

	public String getUserNetWinsCheckHeader() {
		return userNetWinsCheckHeader;
	}

	public void setUserNetWinsCheckHeader(String userNetWinsCheckHeader) {
		this.userNetWinsCheckHeader = userNetWinsCheckHeader;
	}

	public String getUserNetWinsCheckExtension() {
		return userNetWinsCheckExtension;
	}

	public void setUserNetWinsCheckExtension(String userNetWinsCheckExtension) {
		this.userNetWinsCheckExtension = userNetWinsCheckExtension;
	}

	public String getUserNetMarginWinPerCheckHeader() {
		return userNetMarginWinPerCheckHeader;
	}

	public void setUserNetMarginWinPerCheckHeader(String userNetMarginWinPerCheckHeader) {
		this.userNetMarginWinPerCheckHeader = userNetMarginWinPerCheckHeader;
	}

	public String getUserNetMarginWinPerCheckExtension() {
		return userNetMarginWinPerCheckExtension;
	}

	public void setUserNetMarginWinPerCheckExtension(String userNetMarginWinPerCheckExtension) {
		this.userNetMarginWinPerCheckExtension = userNetMarginWinPerCheckExtension;
	}

	public String getMatchmakingSessionCheckHeader() {
		return matchmakingSessionCheckHeader;
	}

	public void setMatchmakingSessionCheckHeader(String matchmakingSessionCheckHeader) {
		this.matchmakingSessionCheckHeader = matchmakingSessionCheckHeader;
	}

	public String getMatchmakingSessionCheckExtension() {
		return matchmakingSessionCheckExtension;
	}

	public void setMatchmakingSessionCheckExtension(String matchmakingSessionCheckExtension) {
		this.matchmakingSessionCheckExtension = matchmakingSessionCheckExtension;
	}

	public String getWinnerCollectionDataHeader() {
		return winnerCollectionDataHeader;
	}

	public void setWinnerCollectionDataHeader(String winnerCollectionDataHeader) {
		this.winnerCollectionDataHeader = winnerCollectionDataHeader;
	}

	public String getWinnerCollectionDataExtension() {
		return winnerCollectionDataExtension;
	}

	public void setWinnerCollectionDataExtension(String winnerCollectionDataExtension) {
		this.winnerCollectionDataExtension = winnerCollectionDataExtension;
	}
}
