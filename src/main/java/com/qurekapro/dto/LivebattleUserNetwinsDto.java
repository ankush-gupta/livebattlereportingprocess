package com.qurekapro.dto;

import org.springframework.data.mongodb.core.mapping.Field;

public class LivebattleUserNetwinsDto {
	
	@Field("uid")
	private long user_id;
	
	@Field("gid")
	private long game_id;
	
	@Field("fid")
	private long format_id;
	
	@Field("fee")
	private int entry_fee;
	
	@Field("net_wins")
	private long net_wins_reached;
	
	private int net_wins_limit;

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getGame_id() {
		return game_id;
	}

	public void setGame_id(long game_id) {
		this.game_id = game_id;
	}

	public long getFormat_id() {
		return format_id;
	}

	public void setFormat_id(long format_id) {
		this.format_id = format_id;
	}

	public int getEntry_fee() {
		return entry_fee;
	}

	public void setEntry_fee(int entry_fee) {
		this.entry_fee = entry_fee;
	}

	public long getNet_wins_reached() {
		return net_wins_reached;
	}

	public void setNet_wins_reached(long net_wins_reached) {
		this.net_wins_reached = net_wins_reached;
	}

	public int getNet_wins_limit() {
		return net_wins_limit;
	}

	public void setNet_wins_limit(int net_wins_limit) {
		this.net_wins_limit = net_wins_limit;
	}
	
}
