package com.qurekapro.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

public class AppCommonUtils {

	public static String generateToken() {
		return UUID.randomUUID().toString();
	}

	public static String getCurrentDate() {
		Calendar cal = Calendar.getInstance();
		TimeZone zone = TimeZone.getTimeZone("IST");
		cal.setTimeZone(zone);
		TimeZone.setDefault(zone);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setTimeZone(zone);
		return dateFormat.format(cal.getTime());
	}

	public static String getCurrentDate_yyyy_MM_dd() {
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
		return dateFormat.format(cal.getTime());
	}

	public static String getYesterDate() {
		Calendar cal = Calendar.getInstance();
		TimeZone zone = TimeZone.getTimeZone("IST");
		cal.setTimeZone(zone);
		TimeZone.setDefault(zone);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		cal.add(Calendar.DATE, -1);
		dateFormat.setTimeZone(zone);
		return dateFormat.format(cal.getTime());
	}

	public static String getYesterDate_yyyy_MM_dd() {
		Calendar cal = Calendar.getInstance();
		TimeZone zone = TimeZone.getTimeZone("IST");
		cal.setTimeZone(zone);
		TimeZone.setDefault(zone);
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
		cal.add(Calendar.DATE, -1);
		dateFormat.setTimeZone(zone);
		return dateFormat.format(cal.getTime());
	}

	public static String getCurrentTime() {
		Calendar cal = Calendar.getInstance();
		TimeZone zone = TimeZone.getTimeZone("IST");
		cal.setTimeZone(zone);
		TimeZone.setDefault(zone);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(zone);
		return dateFormat.format(cal.getTime());
	}
	
	public static int getCurrentHourOfDay() {
		Calendar cal = Calendar.getInstance();
		TimeZone zone = TimeZone.getTimeZone("IST");
		cal.setTimeZone(zone);
		TimeZone.setDefault(zone);
		return cal.get(Calendar.HOUR_OF_DAY);
	}

	public static Date getMidNightTime() {
		Calendar midnight = Calendar.getInstance();
		midnight.set(Calendar.HOUR_OF_DAY, 23);
		midnight.set(Calendar.MINUTE, 59);
		midnight.set(Calendar.SECOND, 59);
		return midnight.getTime();
	}
}
