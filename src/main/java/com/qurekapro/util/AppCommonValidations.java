package com.qurekapro.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppCommonValidations {

	public static final String REGULAR_EXPRESSION_FOR_NUMERIC = "\\d+";

	// private static final Logger LOGS =
	// LoggerFactory.getLogger(AppCommonValidations.class);

	public static boolean checkDeviceValid(String deviceId) {
		return deviceId != null && deviceId.trim().length() >= 10 && deviceId.trim().length() <= 16
				&& isHexadecimal(deviceId.trim()) && !deviceId.matches("[0-9]+") && !deviceId.matches("[a-zA-Z]+");

	}

	public static boolean isHexadecimal(String text) {
		text = text.trim();

		char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B',
				'C', 'D', 'E', 'F' };

		int hexDigitsCount = 0;

		for (char symbol : text.toCharArray()) {
			for (char hexDigit : hexDigits) {
				if (symbol == hexDigit) {
					hexDigitsCount++;
					break;
				}
			}
		}

		return hexDigitsCount == text.length();
	}

	public static boolean checkImeiValid(String imeiNo) {
		int totalValue = 0;
		int tmpValue = 0;

		imeiNo = imeiNo.trim();

		if (imeiNo.matches(REGULAR_EXPRESSION_FOR_NUMERIC) && imeiNo.length() == 15
				&& !(imeiNo.matches("[0]+") || imeiNo.matches("[1]+") || imeiNo.matches("[2]+")
						|| imeiNo.matches("[3]+") || imeiNo.matches("[4]+") || imeiNo.matches("[5]+")
						|| imeiNo.matches("[6]+") || imeiNo.matches("[7]+") || imeiNo.matches("[8]+")
						|| imeiNo.matches("[9]+"))) {

			for (int i = 0; i < imeiNo.length(); i++) {
				if (i % 2 == 0) {
					totalValue += Integer.parseInt(imeiNo.charAt(i) + "");
				} else {
					tmpValue = 2 * Integer.parseInt(imeiNo.charAt(i) + "");
					if (tmpValue >= 10) {
						totalValue += (tmpValue / 10);
						totalValue += (tmpValue % 10);
					} else {
						totalValue += tmpValue;
					}
				}
			}

			return totalValue % 10 == 0;

		}
		return false;
	}

	public static boolean isNumeric(String str) {
		for (char c : str.toCharArray()) {
			if (!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}

	public static boolean validateMobileNo(String mob) {
		boolean st = true;
		if ((mob.substring(0, 1).equals("9")) || (mob.substring(0, 1).equals("8")) || (mob.substring(0, 1).equals("7"))
				|| (mob.substring(0, 1).equals("6")) || (mob.substring(0, 1).equals("5"))) {
			if (mob.length() > 10 || mob.length() < 10) {
				st = false;
			} else {
				for (int i = 0; i < mob.length(); i++) {
					if (!Character.isDigit(mob.charAt(i))) {
						st = false;
						break;
					}
				}
			}
		} else {
			st = false;
		}
		return st;
	}

	public static boolean validateEmailAddress(String emailId) {
		Pattern pattern = Pattern.compile(CommonConstants.EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(emailId);
		return matcher.matches();
	}

}
