package com.qurekapro.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.qurekapro.dto.LivebattleAmountWonPecentDto;
import com.qurekapro.dto.LivebattleUserNetwinsDto;
import com.qurekapro.dto.UserCommonPoolDto;
import com.qurekapro.dto.WinnersDto;
import com.qurekapro.util.CommonConstants;
import com.qurekapro.util.QueryConstants;

@Repository
public class LivebattleDaoImpl implements LivebattleDao {

	private static final Logger logs = LoggerFactory.getLogger(LivebattleDaoImpl.class);

	@Autowired
	@Qualifier("mongoTemplateReporting")
	MongoTemplate mongoTemplateReporting;

	@Autowired
	@Qualifier("primaryDB")
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Map<String, Object>> getNetWinsLimitGameAndFormatWise() {
		try {
			logs.info("fetching net wins limit game-wise format-wise from mysql ...");
			return jdbcTemplate.queryForList(QueryConstants.SELECT_NET_WINS_LIMIT_GAME_WISE_FORMAT_WISE);
		} catch (Exception e) {
			logs.error("Exception in LivebattleDaoImpl getNetWinsLimitGameAndFormatWise :: {}", e.getMessage());
			e.printStackTrace();
		}
		return new ArrayList<>();
	}

	@Override
	public List<LivebattleUserNetwinsDto> getLivebattleUserNetwinsData(String added_date) {
		List<LivebattleUserNetwinsDto> resultList = new ArrayList<>();
		try {
			Query query;
			query = new Query();
			query.addCriteria(Criteria.where("added_date").is(added_date));
			query.with(Sort.by(Sort.Direction.ASC, "gid"));
			logs.info("Query :: {}", query.toString());

			resultList = mongoTemplateReporting.find(query, LivebattleUserNetwinsDto.class,
					CommonConstants.LIVEBATTLE_USER_NET_WINS);
		} catch (Exception e) {
			logs.error("Exception in LivebattleDaoImpl getLivebattleUserNetwinsData :: {}", e.getMessage());
			e.printStackTrace();
		}
		return resultList;
	}

	@Override
	public List<LivebattleAmountWonPecentDto> getLivebattleUserNetMarginAndWinPerData(String added_date) {
		List<LivebattleAmountWonPecentDto> resultList = new ArrayList<>();
		try {
			Query query;
			query = new Query();
			query.addCriteria(Criteria.where("added_date").is(added_date));
			query.with(Sort.by(Sort.Direction.ASC, "uid"));
			logs.info("Query :: {}", query.toString());

			resultList = mongoTemplateReporting.find(query, LivebattleAmountWonPecentDto.class,
					(CommonConstants.LIVE_BATTLE_AMOUNT_WON_PERCENT_COLLECTION));
		} catch (Exception e) {
			logs.error("Exception in LivebattleDaoImpl getLivebattleUserNetMarginAndWinPerData :: {}", e.getMessage());
			e.printStackTrace();
		}
		return resultList;
	}

	@Override
	public List<UserCommonPoolDto> getLivebattleUserMatchMakingSessionData(String added_date) {
		List<UserCommonPoolDto> resultList = new ArrayList<>();
		try {
			Query query;
			query = new Query();
			query.addCriteria(Criteria.where("added_date").is(added_date).and("status")
					.is(CommonConstants.MATCHMAKING_SESSION_STATUS));
			query.with(Sort.by(Sort.Direction.ASC, "gid"));
			logs.info("Query :: {}", query.toString());

			resultList = mongoTemplateReporting.find(query, UserCommonPoolDto.class,
					(CommonConstants.USER_COMMON_POOL_COLLECTION));
		} catch (Exception e) {
			logs.error("Exception in LivebattleDaoImpl getLivebattleUserMatchMakingSessionData :: {}", e.getMessage());
			e.printStackTrace();
		}
		return resultList;
	}

	@Override
	public List<WinnersDto> getLivebattleWinnersCheckReportData(String added_date) {
		List<WinnersDto> resultList = new ArrayList<>();
		try {
			Query query;
			query = new Query();
			query.addCriteria(Criteria.where("added_date").is(added_date).andOperator(
					Criteria.where("user_play_end_reason").in("MAX_SCORE", "MATCH_DURATION", "INCREMENTAL_SCORE")));
			query.with(Sort.by(Sort.Direction.ASC, "user_play_end_reason"));
			logs.info("Query :: {}", query.toString());

			resultList = mongoTemplateReporting.find(query, WinnersDto.class,
					(CommonConstants.WINNERS_TRACK_PREFIX + added_date.replace("-", "_")));
		} catch (Exception e) {
			logs.error("Exception in LivebattleDaoImpl getLivebattleWinnersCheckReportData :: {}", e.getMessage());
			e.printStackTrace();
		}
		return resultList;
	}

}
