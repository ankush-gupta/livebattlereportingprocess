package com.qurekapro.util;

public interface CommonConstants {

	String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	int GENERAL_ERROR_RESPONSE = 400;
	int UNAUTHORIZED_ACCESS = 401;
	int GENERAL_ERROR_RECHARGE = 402;
	int GENERAL_ERROR_BLOCKED = 403;

	int TOURNAMENT_ENDED = 404;
	int TOURNAMENT_USER_LIMIT_ACHIEVED = 405;
	int INVALID_HEADER = 406;

	int INVALI_MOBILE_NO = 406;
	int INVALID_TEMP_ID = 407;
	int INVALID_DEVICE_ID = 408;

	int INSUFFIECIENT_WALLET = 409;

	int INVALID_APK_VERSION = 410;

	int CASH_WINNING_LIMIT_ACHIEVED = 411;

	int REQ_PARAMETER_NOT_FOUND = 412;

	int USER_GAME_ALREADY_REGISTERED = 413;

	int USER_GAME_REGISTERATION_LIMIT_ACHIEVED = 414;

	int EMAIL_VERIFY_REQ_LIMIT_ACHIEVED = 415;

	int EARN_TYPE_CASH = 1;
	int EARN_TYPE_TOKEN = 2;

	/* Pagination limit */
	long WINNER_PAGE_LIMIT = 10;

	int RECORDS_PER_PAGE = 10;

	int MY_TOURNAMENT_PAGE_LIMIT = 10;
	int RECENT_WINNERS_PAGE_LIMIT = 10;
	int TRANSACTION_HISTORY_PAGE_LIMIT = 20;

	int DEBIT_EVENT = 2;
	int CREDIT_EVENT = 1;

	String TOURN_PREFIX = "T_";
	String CASH_TRANSACTIONS = "cash_transactions";
	String TOKEN_TRANSACTIONS = "token_transactions";

	String SINGULAR_EVENTS_COLLECTION = "singular_events";
	String NEW_USER_COLLECTION = "new_users";

	String NEW_USER_TOURN_STATUS = "3";

	String CASH_WALLET_CREDIT = "CREDIT";
	String CASH_WALLET_DEBIT = "DEBIT";

	int REWARD_WALLET_TYPE = 1;
	int PAYTM_WALLET_TYPE = 2;
	int GAME_WALLET_TYPE = 3;
	int PRO_WALLET_TYPE = 4;

	int FREE_CHAMPIONSHIP_TYPE = 4;
	int NEW_USER_CHAMPIONSHIP_TYPE = 5;
	int PRO_CHAMPIONSHIP_TYPE = 13;

	int CHAMPIONSHIP_TYPE_REGULAR = 0;
	int CHAMPIONSHIP_TYPE_PRO = 1;

	int LIVE_BATTLE_CHAMPIONSHIP_TYPE = 6;

	String GAME_PREFIX = "Game_Reg_";
	String NET_WINS_COLLECTION = "user_net_wins";

	String GAME_CONFIG_HASH = "game_configurations";
	String GAME_WIN_LIMIT_HASH = "live_battle_winning_abusers";

	String USER_DATA_KEY = "user_data";
	String WINNERS_TRACK_PREFIX = "winners_";
	String FREE_LIVE_BATTLE_LIMIT_COLLECTION = "free_live_battle_limit";
	String USER_WINNING_SUM_COLLECTION = "user_net_wins";
	String LIVEBATTLE_USER_NET_WINS = "livebattle_user_net_wins";

	// live battle limit
	int FREE_LIVE_BATTLE_LIMIT = 3;
	String LIVE_BATTLE_REGISTRATION = "live_battle_registration";

	String WINNERS_TRACK = "winners_history";

	String WS_AUTH_INFO = "wsAuthInfo";

	String LIVE_BATTLE_USER_AMT_WON_PERCENT = "livebattle_user_amt_won_pecent";
	
	String USER_COMMON_POOL_COLLECTION = "user_common_pool";
	
	 String LIVE_BATTLE_AMOUNT_WON_PERCENT_COLLECTION = "livebattle_user_amt_won_pecent";
	 
	 int MATCHMAKING_SESSION_STATUS = 2;
}
