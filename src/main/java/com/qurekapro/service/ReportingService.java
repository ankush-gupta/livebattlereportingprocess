package com.qurekapro.service;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencsv.CSVWriter;
import com.qurekapro.config.PropertiesConfig;
import com.qurekapro.dao.LivebattleDao;
import com.qurekapro.dto.LivebattleAmountWonPecentDto;
import com.qurekapro.dto.LivebattleUserNetwinsDto;
import com.qurekapro.dto.UserCommonPoolDto;
import com.qurekapro.dto.WinnersDto;
import com.qurekapro.util.AppCommonUtils;

@Service
public class ReportingService {

	private static final Logger logs = LoggerFactory.getLogger(ReportingService.class);

	@Autowired
	PropertiesConfig propConfig;
	
	@Autowired
	LivebattleDao livebattleDao;

	public void generateHourlyReport() {
		
		String added_date;
		int currenthour = AppCommonUtils.getCurrentHourOfDay();
		logs.info("running jar for hour :: {}",currenthour);
		
		if(currenthour == 0) {
			added_date = AppCommonUtils.getYesterDate();
		}else {
			added_date = AppCommonUtils.getCurrentDate();
		}
		logs.info("running jar for date :: {}",added_date);
		
		GetUserNetWinsCheckReport(added_date);
		GetWinnersDataReport(added_date);
		GetUserNetMarginAndWinPerReport(added_date);
		GetMatchMakingSessionCheckReport(added_date);
	}

	//	Report 1 : User net wins check report -------------------------------------------------------------------------------------------------------------------
	private void GetUserNetWinsCheckReport(String added_date) {
		
		// fetching net wins limit from mysql gme wise format wise
		List<Map<String, Object>> netWinsLimitList = livebattleDao.getNetWinsLimitGameAndFormatWise();
		logs.info("net wins limit list size :: {}",netWinsLimitList.size());
		
		Map<String,Integer> net_wins_limit_map = new HashMap<String, Integer>();
		if(!netWinsLimitList.isEmpty()) {
			for (Map<String,Object> map : netWinsLimitList) {
				String key = map.get("game_id").toString()+"_"+map.get("live_battle_id").toString();
				net_wins_limit_map.put(key,(Integer) map.get("net_wins_limit"));
			}
		}
		logs.info("net wins limit mapping game_livebattle id wise :: {}",net_wins_limit_map.toString());
		
		// fetching data for report from mongo
		List<LivebattleUserNetwinsDto> dataList = livebattleDao.getLivebattleUserNetwinsData(added_date);
		logs.info("Fetching GetUserNetWinsCheckReport data size :: {}",dataList.size());
		
		// setting net wins limit of each datalist item 
		if(!net_wins_limit_map.isEmpty() && !dataList.isEmpty()) {
			for (LivebattleUserNetwinsDto dto : dataList) {
				String key = dto.getGame_id() + "_" + dto.getFormat_id();
				if(net_wins_limit_map.containsKey(key)) {
					dto.setNet_wins_limit(net_wins_limit_map.get(key));
				}
			}
		}		
		
		String filename = propConfig.getReportExportUrl() + added_date.replace("-", "_") + propConfig.getUserNetWinsCheckExtension();
		String [] header = propConfig.getUserNetWinsCheckHeader().toString().split(",");
		writeCsvFile1(dataList,filename,header);
	}
	
	private void writeCsvFile1(List<LivebattleUserNetwinsDto> dataList, String fileName, String[] header) {
		logs.info("Generating Csv ...");
		File csvFile = new File(fileName);
		CSVWriter csvWriter;
		try {
				csvWriter = new CSVWriter(new FileWriter(csvFile));
				csvWriter.writeNext(header);
				
				for (LivebattleUserNetwinsDto dto : dataList) {
					String[] record = (String.valueOf(dto.getUser_id()) + "," + String.valueOf(dto.getEntry_fee()) +
							"," + String.valueOf(dto.getGame_id()) + "," + String.valueOf(dto.getFormat_id()) + "," + String.valueOf(dto.getNet_wins_reached()) +
							"," + String.valueOf(dto.getNet_wins_limit())
							).split(",");
					csvWriter.writeNext(record);
				}
				csvWriter.flush();
				csvWriter.close();
				logs.info("Csv is generated at path :: {}",fileName);
		} catch (Exception e) {
			logs.error("Exception in ReportingService writeCsvFile1 for filename:: {} , {}",fileName,e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	
	//	Report 2 : Max Possible Score Check & Match duration Check & Incremental Score Check Report ----------------------------------------------------------------------
	private void GetWinnersDataReport(String added_date) {
		List<WinnersDto> dataList = livebattleDao.getLivebattleWinnersCheckReportData(added_date);
		logs.info("Fetching GetWinnersDataReport data size :: {}",dataList.size());
		String filename = propConfig.getReportExportUrl() + added_date.replace("-", "_") + propConfig.getWinnerCollectionDataExtension();
		String [] header = propConfig.getWinnerCollectionDataHeader().toString().split(",");
		writeCsvFile2(dataList,filename,header);
	}

	private void writeCsvFile2(List<WinnersDto> dataList, String fileName, String[] header) {
		logs.info("Generating Csv ...");
		File csvFile = new File(fileName);
		CSVWriter csvWriter;
		try {
				csvWriter = new CSVWriter(new FileWriter(csvFile));
				csvWriter.writeNext(header);
				
				for (WinnersDto dto : dataList) {
					String[] record = (String.valueOf(dto.getUser_id()) + "," + String.valueOf(dto.getGid()) +
							"," + String.valueOf(dto.getTournament_id()) + "," + String.valueOf(dto.getUser_play_end_reason())+","+String.valueOf(dto.getPartner_id())
							).split(",");
					csvWriter.writeNext(record);
				}
				csvWriter.flush();
				csvWriter.close();
				logs.info("Csv is generated at path :: {}",fileName);
		} catch (Exception e) {
			logs.error("Exception in ReportingService writeCsvFile2 for filename:: {} , {}",fileName,e.getMessage());
			e.printStackTrace();
		}
	}	
	
	
//	Report 3 : User net margins and win percentage check report -------------------------------------------------------------------------------------------------------------------
	private void GetUserNetMarginAndWinPerReport(String added_date) {
		List<LivebattleAmountWonPecentDto> dataList = livebattleDao.getLivebattleUserNetMarginAndWinPerData(added_date);
		logs.info("Fetching GetUserNetMarginAndWinPerReport data size :: {}",dataList.size());
		String filename = propConfig.getReportExportUrl() + added_date.replace("-", "_") + propConfig.getUserNetMarginWinPerCheckExtension();
		String [] header = propConfig.getUserNetMarginWinPerCheckHeader().toString().split(",");
		writeCsvFile3(dataList,filename,header);
	}

	private void writeCsvFile3(List<LivebattleAmountWonPecentDto> dataList, String filename, String[] header) {
		logs.info("Generating Csv ...");
		File csvFile = new File(filename);
		CSVWriter csvWriter;
		try {
				csvWriter = new CSVWriter(new FileWriter(csvFile));
				csvWriter.writeNext(header);
				
				for (LivebattleAmountWonPecentDto dto : dataList) {
					String[] record = (String.valueOf(dto.getUid()) + "," + String.valueOf(dto.getWins()) +
							"," + String.valueOf(dto.getLoses()) + "," + String.valueOf(dto.getNet_wins()) +    
							"," + String.valueOf(dto.getTot_enty_fee()) + "," + String.valueOf(dto.getTot_won_amount()) 	
							).split(",");
					csvWriter.writeNext(record);
				}
				csvWriter.flush();
				csvWriter.close();
				logs.info("Csv is generated at path :: {}",filename);
		} catch (Exception e) {
			logs.error("Exception in ReportingService writeCsvFile3 for filename:: {} , {}",filename,e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	
//	Report 4 : User match making check report -------------------------------------------------------------------------------------------------------------------
	private void GetMatchMakingSessionCheckReport(String added_date) {
		List<UserCommonPoolDto> dataList = livebattleDao.getLivebattleUserMatchMakingSessionData(added_date);
		logs.info("Fetching GetMatchMakingSessionCheckReport data size :: {}",dataList.size());
		String filename = propConfig.getReportExportUrl() + added_date.replace("-", "_") + propConfig.getMatchmakingSessionCheckExtension();
		String [] header = propConfig.getMatchmakingSessionCheckHeader().toString().split(",");
		writeCsvFile4(dataList,filename,header);
	}

	private void writeCsvFile4(List<UserCommonPoolDto> dataList, String fileName, String[] header) {
		logs.info("Generating Csv ...");
		File csvFile = new File(fileName);
		CSVWriter csvWriter;
		try {
				csvWriter = new CSVWriter(new FileWriter(csvFile));
				csvWriter.writeNext(header);
				
				for (UserCommonPoolDto dto : dataList) {
					String[] record = (String.valueOf(dto.getUid()) + "," + String.valueOf(dto.getGid()) +
							"," + String.valueOf(dto.getFid()) + "," + String.valueOf(dto.getCreated()) ).split(",");
					csvWriter.writeNext(record);
				}
				csvWriter.flush();
				csvWriter.close();
				logs.info("Csv is generated at path :: {}",fileName);
		} catch (Exception e) {
			logs.error("Exception in ReportingService writeCsvFile4 for filename:: {} , {}",fileName,e.getMessage());
			e.printStackTrace();
		}		
	}
}
