package com.qurekapro.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClientURI;

@Configuration
public class MongoConfig {

	@Autowired
	DatabaseProperties databaseProperties;

	@Primary
	public @Bean(name = "mongoTemplateReporting") MongoTemplate mongoTemplateReporting() throws Exception {
		return new MongoTemplate(
				new SimpleMongoDbFactory(new MongoClientURI(databaseProperties.getReporting_mongo_db_url())));
	}

}
