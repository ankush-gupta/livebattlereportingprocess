package com.qurekapro.dto;

import org.springframework.data.mongodb.core.mapping.Field;

public class WinnersDto {

	private String user_id;

	@Field("gid")
	private String gid;

	@Field("tid")
	private long tournament_id;

	private String partner_id;

	private String user_play_end_reason;

	public String getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(String partner_id) {
		this.partner_id = partner_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public long getTournament_id() {
		return tournament_id;
	}

	public void setTournament_id(long tournament_id) {
		this.tournament_id = tournament_id;
	}

	public String getUser_play_end_reason() {
		return user_play_end_reason;
	}

	public void setUser_play_end_reason(String user_play_end_reason) {
		this.user_play_end_reason = user_play_end_reason;
	}
}
