package com.qurekapro.util;

public interface QueryConstants {
	String GET_TOKEN_DAYS = "SELECT user_id,DATEDIFF(current_date, added_date) AS days FROM user_token where user_id=? and token=? and is_active=1";
	String GET_RANK_GRID = "SELECT live_battle_id as lb_id, max_rank as mxr, min_rank as mir, prize_money as pm FROM live_battle_rank_grid WHERE live_battle_id in (?)";
	String GET_TOUR_DETAIL = "SELECT gid, B.name as game_name, format_name, format_type, format_max_avg_score, users, prize_money_cash, prize_money_token, entry_fee, entry_fee_type, bonus_amt, A.max_score FROM live_battle_formats A LEFT JOIN games B on B.id = A.gid WHERE live_battle_id = ? AND is_active = 'Y' LIMIT 1";
	String GET_NET_WINS_LIMIT = "SELECT entry_fee, net_wins_limit FROM net_wins_configuration where game_id = ? AND fid = ?";
	String GET_TOKEN_WALLET = "SELECT user_id,game_earned,reward_earned,(game_earned+reward_earned) balance,total_earned,game_redeemed,reward_redeemed FROM token_wallet WHERE user_id=?";
	String GET_CASH_WALLET = "SELECT c.user_id,c.game_earned,c.reward_earned,(c.game_earned+c.reward_earned+c.paytm_deposit+ifnull(pro_earned,0)+ifnull(s.game_earned,0)) balance,c.total_earned,c.paytm_deposit,c.game_redeemed,c.paytm_redeemed,c.reward_redeemed,ifnull(pro_earned,0) pro_earned,ifnull(pro_redeemed,0) pro_redeemed,ifnull(s.game_earned,0) sports_earned,ifnull(s.game_redeemed,0) sports_redeemed  FROM cash_wallet c left join pro_users_wallet p on (c.user_id=p.user_id) left join sports_wallet s on (c.user_id=s.user_id) WHERE c.user_id=?";
	String GET_GAME_CONFIG = "SELECT value FROM game_configurations WHERE config LIKE ?";

	// Updates
	String UPDATE_CASH_WALLET_ON_GAME_REGIS = "UPDATE cash_wallet SET reward_earned = reward_earned - :reward_redeemed, reward_redeemed = reward_redeemed + :reward_redeemed, game_earned = game_earned - :game_redeemed, game_redeemed = game_redeemed + :game_redeemed, paytm_deposit = paytm_deposit - :paytm_redeemed, paytm_redeemed = paytm_redeemed + :paytm_redeemed, modified = NOW() WHERE user_id = :user_id";
	String UPDATE_TOKEN_WALLET_ON_GAME_REGIS = "UPDATE token_wallet SET reward_earned = reward_earned - :reward_redeemed, reward_redeemed = reward_redeemed + :reward_redeemed, game_earned = game_earned - :game_redeemed, game_redeemed = game_redeemed + :game_redeemed, modified = NOW() WHERE user_id = :user_id";
	String REG_FROM_PRO_WALLET = "UPDATE pro_users_wallet SET pro_earned=pro_earned - ?, pro_redeemed=pro_redeemed + ?, modified = now() WHERE user_id = ? AND pro_earned >= ?";

	String GET_GAME_DETAILS = "SELECT  name, image as img, banner_img, image_2 as img_2, how_to_play_url as htp_url, url, orientation_flag, check_sum_files as game_files, round_1 as r1 , round_2 as r2  FROM games WHERE id = ? LIMIT 1";
	String GET_LIVEBATTLE_NET_WINS = "SELECT net_wins_limit FROM live_battle_net_wins_configuration WHERE game_id = ? AND live_battle_id = ? LIMIT 1";

	String GET_GAME_TOURNAMENT_INFO = "SELECT t.id tournament_id, game_id, t.name tournament_name,g.name game_name,t.entry_fee, t.entry_fee_type,t.start_time,t.end_time,g.image game_image,t.prize_money,g.orientation_flag,t.score_flag,g.score_share_url,g.banner_img FROM tournaments t left join games g on t.game_id=g.id WHERE t.id=?";

	String SELECT_NET_WINS_LIMIT_GAME_WISE_FORMAT_WISE = "SELECT `game_id`, `live_battle_id`, `net_wins_limit` FROM `live_battle_net_wins_configuration` WHERE 1";
}
